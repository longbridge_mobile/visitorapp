webpackJsonp([0],{

/***/ 289:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitorPageModule", function() { return VisitorPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__visitor__ = __webpack_require__(295);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var VisitorPageModule = (function () {
    function VisitorPageModule() {
    }
    VisitorPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__visitor__["a" /* VisitorPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["e" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__visitor__["a" /* VisitorPage */]),
            ],
        })
    ], VisitorPageModule);
    return VisitorPageModule;
}());

//# sourceMappingURL=visitor.module.js.map

/***/ }),

/***/ 295:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return VisitorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__ = __webpack_require__(101);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_shared_controller_shared_controller__ = __webpack_require__(201);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var VisitorPage = (function () {
    function VisitorPage(navCtrl, navParams, alertCtrl, modalCtrl, backend, shared) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.backend = backend;
        this.shared = shared;
        this.name = '';
        this.company = '';
        this.phoneNumber = '';
        this.email = '';
        this.purpose = '';
        this.image = '';
        this.id = '';
    }
    VisitorPage.prototype.ionViewDidLoad = function () {
        this.name = this.navParams.get('name');
        this.company = this.navParams.get('company');
        this.phoneNumber = this.navParams.get('phoneNumber');
        this.email = this.navParams.get('email');
        this.purpose = this.navParams.get('purpose');
        this.image = this.navParams.get('image');
        this.id = this.navParams.get('id');
    };
    VisitorPage.prototype.accept = function () {
        this.showAlert('Enter note', 'Leave a note', 'Accept Visitor', 'accept');
    };
    VisitorPage.prototype.reject = function () {
        this.showAlert('Reason for rejection', 'Enter your reason for rejecting this visitor', 'Reject Visitor', 'reject');
    };
    VisitorPage.prototype.reassign = function () {
        var _this = this;
        var modalOptions = {
            enableBackdropDismiss: true,
        };
        var modal = this.modalCtrl.create('StafflistPage', '', modalOptions);
        modal.onDidDismiss(function (data) {
            if (data) {
                _this.staffId = data.id.toString();
                _this.showAlert('Reason for reassigning', 'Enter your reason for reassigning this visitor', 'Reassign Visitor', 'reassign');
            }
        });
        modal.present();
    };
    VisitorPage.prototype.showAlert = function (placeholder, message, title, type) {
        var _this = this;
        var alertOptions = {
            title: title,
            message: message,
            inputs: [
                {
                    name: 'reason',
                    placeholder: placeholder,
                    id: 'reason',
                    type: 'text'
                }
            ],
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel'
                },
                {
                    text: 'Submit',
                    handler: function () {
                        alert.onDidDismiss(function (data) {
                            if (type === 'reject')
                                _this.updateVisitor(type, '', data.reason);
                            if (type === 'reassign')
                                _this.updateVisitor(type, _this.staffId, data.reason);
                            if (type === 'accept')
                                _this.updateVisitor(type, '', data.reason);
                        });
                    }
                }
            ]
        };
        var alert = this.alertCtrl.create(alertOptions);
        alert.present();
    };
    VisitorPage.prototype.updateVisitor = function (action, newHost, reason) {
        return __awaiter(this, void 0, void 0, function () {
            var body, loader, response, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        body = {
                            visitorLog: this.id.toString(),
                            action: action,
                            newHost: newHost,
                            reason: reason
                        };
                        loader = this.shared.showLoader();
                        loader.present();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, , 4]);
                        return [4 /*yield*/, this.backend.updateVisitor(body)];
                    case 2:
                        response = _a.sent();
                        if (response.status === 'OO') {
                            if (action === 'accept')
                                this.shared.showAlert('Your Visitor will be with you shortly!');
                            if (action === 'reject')
                                this.shared.showAlert('Your Visitor will be notified of your unavailability at this time');
                            if (action === 'reassign')
                                this.shared.showAlert('Your Visitor has been reassigned as requested');
                            this.navCtrl.setRoot('HomePage');
                        }
                        else {
                            this.shared.showAlert(response.message);
                        }
                        return [3 /*break*/, 4];
                    case 3:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 4];
                    case 4:
                        loader.dismiss();
                        return [2 /*return*/];
                }
            });
        });
    };
    VisitorPage.prototype.goBack = function () {
        this.navCtrl.pop();
    };
    VisitorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-visitor',template:/*ion-inline-start:"/Users/macbookpro/Desktop/Longbridge/visitorapp/src/pages/visitor/visitor.html"*/'<ion-header>\n  <ion-navbar color="primary"></ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n  <ion-list no-lines>\n\n    <ion-item class="visitor-name">\n      <img [src]="image" item-start height="100" width="100">\n      <div><strong>{{name}}</strong></div>\n      <p>{{company}}</p>\n    </ion-item>\n\n    <ion-item class="card-container">\n      <ion-card>\n        <ion-card-header>\n          <strong>Details</strong>\n        </ion-card-header>\n\n        <ion-list>\n          <ion-item class="details-item">\n            <h3>Phone Number</h3>\n            <p>{{phoneNumber}}</p>\n          </ion-item>\n\n          <ion-item class="details-item">\n            <h3>Email</h3>\n            <p>{{email}}</p>\n          </ion-item>\n\n          <ion-item class="details-item">\n            <h3>Purpose of Visit</h3>\n            <p>{{purpose}}</p>\n          </ion-item>\n        </ion-list>\n      </ion-card>\n    </ion-item>\n\n    <ion-row>\n      <ion-col (click)="accept()">\n        <ion-icon color="primary" name="ios-checkmark-circle-outline"></ion-icon>\n        <br>\n        <span>Accept</span>\n      </ion-col>\n\n      <ion-col (click)="reject()">\n        <ion-icon color="danger" name="ios-close-circle-outline"></ion-icon>\n        <br>\n        <span>Reject</span>\n      </ion-col>\n\n      <ion-col (click)="reassign()">\n        <ion-icon color="secondary" name="ios-refresh-circle-outline"></ion-icon>\n        <br>\n        <span color="secondary">Reassign</span>\n      </ion-col>\n    </ion-row>\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"/Users/macbookpro/Desktop/Longbridge/visitorapp/src/pages/visitor/visitor.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_backend_backend__["a" /* BackendProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_shared_controller_shared_controller__["a" /* SharedControllerProvider */]])
    ], VisitorPage);
    return VisitorPage;
}());

//# sourceMappingURL=visitor.js.map

/***/ })

});
//# sourceMappingURL=0.js.map