import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { IonicStorageModule } from '@ionic/storage';

import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackgroundMode } from '@ionic-native/background-mode';
import { HeaderColor } from '@ionic-native/header-color';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { FilePath } from '@ionic-native/file-path';

import { MyApp } from './app.component';
import { BackendProvider } from '../providers/backend/backend';
import { SharedControllerProvider } from '../providers/shared-controller/shared-controller';

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp
  ],
  providers: [
    StatusBar,
    SplashScreen,
    LocalNotifications,
    BackgroundMode,
    HeaderColor,
    BarcodeScanner,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BackendProvider,
    SharedControllerProvider
  ]
})
export class AppModule {}
