import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LocalNotifications } from '@ionic-native/local-notifications';
import { BackendProvider } from '../providers/backend/backend';
import { BackgroundMode } from '@ionic-native/background-mode';
import { HeaderColor } from '@ionic-native/header-color';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: string = '';

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private localNotify: LocalNotifications,
    private backend: BackendProvider,
    backgroundMode: BackgroundMode,
    headerColor: HeaderColor
  ) {
    platform.ready().then(() => {
      statusBar.overlaysWebView(false);
      (platform.is('android')) ? statusBar.backgroundColorByHexString('#002C4D') : statusBar.backgroundColorByHexString('#002C4D');
      headerColor.tint('#002C4D');
      splashScreen.hide();
      this.setRootPage();
      backgroundMode.enable();

      setInterval(()=> {
        this.checkForVisitor();
      }, 10000);
    });
  }

  setRootPage(): void {
    (localStorage.getItem('otp') === 'true') ? this.rootPage = 'HomePage' : this.rootPage = 'LandingPage';
  }

  async checkForVisitor() {
    try {
      if (localStorage.getItem('staffId')) {
        let response = await this.backend.getVisitors(localStorage.getItem('staffId'), localStorage.getItem('sessionId'));

        if (response.status === 'OO') {
          this.localNotify.schedule({
            id: 1,
            led: 'FF0000',
            text: 'You have a waiting visitor',
            sound: 'assets/sound/alert.mp3'
          });

          this.localNotify.on('click', ()=> {
            this.rootPage = 'HomePage';
          });
        }
      }
    } catch(err) {
      console.log(err);
    }
  }
}
