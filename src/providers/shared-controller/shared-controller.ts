import { Injectable } from '@angular/core';
import { LoadingController, LoadingOptions } from 'ionic-angular';
import { ToastController, ToastOptions } from 'ionic-angular';

@Injectable()
export class SharedControllerProvider {

  constructor(private loadingCtrl: LoadingController, private toastCtrl: ToastController) {
    console.log('Hello SharedControllerProvider Provider');
  }

  showLoader(): any {
    let loadOptions: LoadingOptions = {
      content: 'Please wait...'
    };

    return this.loadingCtrl.create(loadOptions);
  }

  showAlert(message): any {
    let toastOptions: ToastOptions = {
      message: message,
      duration: 2000
    };

    this.toastCtrl.create(toastOptions).present();
  }

}
