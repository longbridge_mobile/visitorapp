import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Storage } from '@ionic/storage';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class BackendProvider {
  // url: string = '/api';
  url: string = 'https://vmslongbridge.herokuapp.com/api';
  headers: Headers = new Headers();
  failure: any = {
    message: 'Unable to connect',
    status: 'FF'
  };

  constructor(public http: Http, private storage: Storage) {
    this.headers.append('Content-Type', 'application/json');
  }

  async validateStaff(staffId) {
    try {
      let response = await this.http.post(`${this.url}/${staffId}/validateHost`, {}, {headers: this.headers}).toPromise();
      return response.json();
    } catch (err) {
      console.log(err.json());
      return err.json();
    }
  }

  async validateToken(staffId, token) {
    try {
      let response = await this.http.post(`${this.url}/${staffId}/${token}/validateToken`, {}, {headers: this.headers}).toPromise();
      return response.json();
    } catch (err) {
      console.log(err);
      return err.json();
    }
  }

  async getVisitors(staffId, sessionId) {
    try {
      let response = await this.http.post(`${this.url}/${staffId}/${sessionId}/getVisitors`, {}, {headers: this.headers}).toPromise();
      return response.json();
    } catch (err) {
      console.log(err);
      return err.json();
    }
  }

  async getAllStaff() {
    try {
      let response = await this.http.get(`${this.url}/getallhost`, {headers: this.headers}).toPromise();
      return response;
    } catch (err) {
      console.log(err);
      return err.json();
    }
  }

  async updateVisitor(body) {
    console.log(`the url ${this.url}`);
    try {
      let response = await this.http.post(`${this.url}/${localStorage.getItem('sessionId')}/updateVisitor`, JSON.stringify(body), {headers: this.headers}).toPromise();
      return response.json();
    } catch (err) {
      console.log(err);
      return err.json();
    }
  }

  async getUrl() {
    let response = await this.storage.get('api');
    return response;
  }

}
