import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { SharedControllerProvider } from '../../providers/shared-controller/shared-controller';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';

@IonicPage()
@Component({
  selector: 'page-user-login',
  templateUrl: 'user-login.html',
})
export class UserLoginPage {
  staffId: string = '';
  barcodeResult: string = '';

  constructor(
    public navCtrl: NavController,
    private backend: BackendProvider,
    private sharedController: SharedControllerProvider,
    public platform: Platform,
    private barcode: BarcodeScanner
  ) { }

  async proceed() {
    if (!this.staffId) {
      alert('Staff Id is required');
      return false;
    }
    
    let loader = this.sharedController.showLoader();
    loader.present();

    try {
      let response = await this.backend.validateStaff(this.staffId);
      console.log(response);

      if (response.status === '00') {
        localStorage.setItem('sessionId', response.host.sessionId);
        localStorage.setItem('staffId', this.staffId);
        localStorage.setItem('name', response.host.name);
        this.navCtrl.push('OtpPage');
      } else {
        this.sharedController.showAlert(response.message);
      }
    } catch (err) {
      console.log(err);
    }

    loader.dismiss();
  }

  scanBarcode() {
    let barcodeOptions: BarcodeScannerOptions = {
      showTorchButton: true,
      orientation: 'portrait',
      showFlipCameraButton: true
    };

    this.barcode.scan(barcodeOptions).then((val) => {
      this.barcodeResult = val.text;
    }).catch((err) => {
      console.log(err);
    });
  }

  goBack(): void {
    this.navCtrl.pop();
  }
}
