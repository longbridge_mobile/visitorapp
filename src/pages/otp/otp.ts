import { Component } from '@angular/core';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { SharedControllerProvider } from '../../providers/shared-controller/shared-controller';

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  otpString: string = '';

  constructor(
    public navCtrl: NavController,
    private backend: BackendProvider,
    private sharedController: SharedControllerProvider,
    public platform: Platform
  ) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }

  async proceed() {
    if (!this.otpString) {
      alert('Code is required');
      return false;
    }
    
    let loader = this.sharedController.showLoader();
    loader.present();
    try {
      let response = await this.backend.validateToken(localStorage.getItem('staffId'), this.otpString);
      console.log(response);

      (response.status === '00') ? this.otpSuccess() : this.sharedController.showAlert(response.message);
    } catch(err) {
      console.log(err);
    }
    loader.dismiss();
  }

  otpSuccess(): void {
    localStorage.setItem('otp', 'true');
    this.navCtrl.setRoot('HomePage');
  }

  goBack(): void {
    this.navCtrl.pop();
  }

}
