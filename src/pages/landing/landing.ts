import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AlertController, AlertOptions } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html',
})
export class LandingPage {

  constructor(
    public navCtrl: NavController, 
    private alertCtrl: AlertController, 
    private storage: Storage
  ) {
  }

  async ionViewDidLoad() {
    // (await this.storage.get('api')) ? this.revealAPI(await this.storage.get('api')) : this.takeAPI();
  }

  getStarted(): void {
    this.navCtrl.push('UserLoginPage');
  }

  takeAPI(): void {
    let alertOptions: AlertOptions = {
      message: 'Enter your api endpoint',
      inputs: [
        {
          id: 'api',
          placeholder: 'e.g. http:127.0.0.1:3000/api',
          type: 'url',
          label: 'API Endpoint',
          name: 'api'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Submit',
          handler: () => {
            alert.onDidDismiss(async (data) => {
              await this.storage.set('api', data.api);
            });
          }
        }
      ]
    };

    let alert = this.alertCtrl.create(alertOptions);
    alert.present();
  }

  revealAPI(apiString): void {
    let alertOptions: AlertOptions = {
      message: 'Do you want to change your current api endpoint?',
      subTitle: apiString,
      buttons: [
        {
          text: 'No',
          role: 'cancel'
        },
        {
          text: 'Yes',
          handler: () => {
            this.takeAPI();
          }
        }
      ]
    };

    let alert = this.alertCtrl.create(alertOptions);
    alert.present();
  }

}
