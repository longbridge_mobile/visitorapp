import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController, AlertOptions } from 'ionic-angular';
import { ModalController, ModalOptions } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { SharedControllerProvider } from '../../providers/shared-controller/shared-controller';

@IonicPage()
@Component({
  selector: 'page-visitor',
  templateUrl: 'visitor.html',
})
export class VisitorPage {
  visitorDetails: any;
  name: string = '';
  company: string = '';
  phoneNumber: string = '';
  email: string = '';
  purpose: string = '';
  image: string = '';
  id: string = '';
  staffId: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private modalCtrl: ModalController,
    private backend: BackendProvider,
    private shared: SharedControllerProvider
  ) { }

  ionViewDidLoad() {
    this.name = this.navParams.get('name');
    this.company = this.navParams.get('company');
    this.phoneNumber = this.navParams.get('phoneNumber');
    this.email = this.navParams.get('email');
    this.purpose = this.navParams.get('purpose');
    this.image = this.navParams.get('image');
    this.id = this.navParams.get('id');
  }

  accept(): void {
    this.showAlert('Enter note', 'Leave a note', 'Accept Visitor', 'accept');
  }

  reject(): void {
    this.showAlert('Reason for rejection', 'Enter your reason for rejecting this visitor', 'Reject Visitor', 'reject');
  }

  reassign(): void {
    const modalOptions: ModalOptions = {
      enableBackdropDismiss: true,
    }

    let modal = this.modalCtrl.create('StafflistPage', '', modalOptions);
    modal.onDidDismiss((data) => {
      if (data) {
        this.staffId = data.id.toString();
        this.showAlert('Reason for reassigning', 'Enter your reason for reassigning this visitor', 'Reassign Visitor', 'reassign');
      }
    });
    modal.present();
  }

  showAlert(placeholder: string, message: string, title: string, type: string) {
    let alertOptions: AlertOptions = {
      title: title,
      message: message,
      inputs: [
        {
          name: 'reason',
          placeholder: placeholder,
          id: 'reason',
          type: 'text'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Submit',
          handler: () => {
            alert.onDidDismiss((data) => {
              if (type === 'reject') this.updateVisitor(type, '', data.reason);
              if (type === 'reassign') this.updateVisitor(type, this.staffId, data.reason);
              if (type === 'accept') this.updateVisitor(type, '', data.reason);
            });
          }
        }
      ]
    };
    let alert = this.alertCtrl.create(alertOptions);
    alert.present();
  }

  async updateVisitor(action: string, newHost: string, reason: string) {
    let body = {
      visitorLog: this.id.toString(),
      action: action,
      newHost: newHost,
      reason: reason
    };

    let loader = this.shared.showLoader();
    loader.present();

    try {
      let response = await this.backend.updateVisitor(body);
      if (response.status === 'OO') {
        if (action === 'accept') this.shared.showAlert('Your Visitor will be with you shortly!');
        if (action === 'reject') this.shared.showAlert('Your Visitor will be notified of your unavailability at this time');
        if (action === 'reassign') this.shared.showAlert('Your Visitor has been reassigned as requested');
        this.navCtrl.setRoot('HomePage');
      } else {
        this.shared.showAlert(response.message);
      }
    }
    catch (err) {
      console.log(err);
    }

    loader.dismiss();
  }

  goBack(): void {
    this.navCtrl.pop();
  }

}
