import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, Platform } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { SharedControllerProvider } from '../../providers/shared-controller/shared-controller';

@IonicPage()
@Component({
  selector: 'page-stafflist',
  templateUrl: 'stafflist.html',
})
export class StafflistPage {
  staffData: Array<any> = [];
  _staffData: Array<any> = [];

  constructor(
    public navCtrl: NavController,
    private viewCtrl: ViewController,
    private backend: BackendProvider,
    private shared: SharedControllerProvider,
    public platform: Platform
  ) {}

  ionViewDidLoad() {
    this.initializeArray();
  }

  async initializeArray() {
    let loader = this.shared.showLoader();
    loader.present();

    try {
      let response = await this.backend.getAllStaff();
      if (response.status === 200) {
        this.staffData = response.json();
        this._staffData = response.json();
      }
      else {
        this.shared.showAlert("Couldn't get list of staff");
      }
    } catch(err) {
      console.log(err);
    }

    loader.dismiss();
  }

  onCancel(): void {
    this.viewCtrl.dismiss('');
  }

  dismissView(data): void {
    this.viewCtrl.dismiss(data);
  }

  getItems(ev): void {
    this.staffData = this._staffData;
    var val = ev.target.value;
  
    if (val && val.trim() != '') {
      this.staffData = this.staffData.filter((item) => {
        return (item.name.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  selectedStaff(val): void {
    this.viewCtrl.dismiss(val);
  }

}
