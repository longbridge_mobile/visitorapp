import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BackendProvider } from '../../providers/backend/backend';
import { SharedControllerProvider } from '../../providers/shared-controller/shared-controller';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  visitors: Array<any> = [];
  userName: string = '';
  currentDate: string = new Date().toDateString();

  constructor(
    public navCtrl: NavController,
    private shared: SharedControllerProvider,
    private backend: BackendProvider
  ) {}

  ionViewDidLoad() {
  }

  ionViewDidEnter() {
    this.userName = localStorage.getItem('name');
    this.getVisitors();
  }

  async getVisitors() {
    let loader = this.shared.showLoader();
    loader.present();
    try {
      let response = await this.backend.getVisitors(localStorage.getItem('staffId'), localStorage.getItem('sessionId'));
      console.log(response);
      (response.status === 'OO') ? this.visitors = this.mydata(response.waitingVisitorDTOS) : '';
    } catch (err) {
      console.log(err);
    }
    loader.dismiss();
  }

  mydata(ev): Array<any> {
    let myArray: Array<any> = [];
    for (let val of ev) {
      myArray.push({
        id: val.id,
        name: val.visitor.name,
        company: val.visitor.company,
        purpose: val.purpose,
        phoneNumber: val.visitor.phoneNumber,
        email: val.visitor.email,
        image: val.visitor.image
      });
    }

    return myArray;
  }

  viewVisitor(data): void {
    this.navCtrl.push('VisitorPage', {
      name: data.name,
      company: data.company,
      purpose: data.purpose,
      phoneNumber: data.phoneNumber,
      email: data.email,
      image: data.image,
      id: data.id
    });
  }

}
